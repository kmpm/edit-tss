module edittss

go 1.21.3

require (
	github.com/lavoqualis/urmsp.go v0.0.5
	github.com/nats-io/jsm.go v0.1.0
	github.com/nats-io/nats.go v1.31.0
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/nats-io/nkeys v0.4.5 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/rs/zerolog v1.29.1 // indirect
	go.bug.st/serial v1.5.0 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
