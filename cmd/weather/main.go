package main

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"time"

	"github.com/nats-io/jsm.go/natscontext"
	"github.com/nats-io/nats.go"
)

// CountMsg is a message sent from counter service
type CountMsg struct {
	Epc   string
	Count int
}

func main() {
	// connect to nats
	nc := connectNATS("nats_development")
	defer nc.Close()
	slog.Info("connected to nats", "url", nc.ConnectedUrl())

	// subscribe to rfid.count
	chanMsg := make(chan *nats.Msg)
	nc.ChanSubscribe("rfid.count", chanMsg)
	slog.Info("subscribed to topic", "topic", "rfid.count")

	for msg := range chanMsg {
		// unmarshal message
		in := CountMsg{}
		err := json.Unmarshal(msg.Data, &in)
		check(err)
		// check weather if specific epc is found
		if in.Epc == "E20034120137FF000A69751D" {
			go checkWeather(nc, "Gothenburg")
		}
	}
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// connect to nats using context name and returns a nats connection
func connectNATS(name string) *nats.Conn {
	nc, err := natscontext.Connect(name)
	check(err)
	return nc
}

func checkWeather(nc *nats.Conn, city string) {
	subject := fmt.Sprintf("service.weather.%s", city)
	timeout := 5 * time.Second
	slog.Debug("checking weather", "city", city)
	response, err := nc.Request(subject, nil, timeout)
	if err != nil {
		slog.Error("error requesting weather", "city", city, "error", err)
		return
	}
	slog.Info("received weather", "city", city, "weather", string(response.Data))

}
