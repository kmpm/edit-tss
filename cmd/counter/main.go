package main

import (
	"encoding/json"
	"log/slog"

	"github.com/nats-io/jsm.go/natscontext"
	"github.com/nats-io/nats.go"
)

var counters = make(map[string]int)
var total int

func check(err error) {
	if err != nil {
		panic(err)
	}
}

type CountMsg struct {
	Epc   string
	Count int
}

func main() {
	nc := connectNATS("nats_development")
	defer nc.Close()
	slog.Info("connected to nats", "url", nc.ConnectedUrl())

	chanMsg := make(chan *nats.Msg)
	nc.ChanSubscribe("rfid.epc.>", chanMsg)
	slog.Info("subscribed to topic", "topic", "rfid.epc.>")

	for msg := range chanMsg {
		epc := string(msg.Data)
		if _, ok := counters[epc]; !ok {
			counters[epc] = 1
			total++
		} else {
			counters[epc]++
		}
		value := counters[epc]
		slog.Info("received message", "epc", epc, "count", value, "total", total)

		if value%10 == 0 {
			out := CountMsg{Epc: epc, Count: value}
			data, err := json.Marshal(out)
			check(err)
			nc.Publish("rfid.count", data)

		}
	}
}

// connect to nats using context name
func connectNATS(name string) *nats.Conn {
	nc, err := natscontext.Connect(name)
	check(err)
	return nc
}
