package main

import (
	"context"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/lavoqualis/urmsp.go"
	"github.com/nats-io/jsm.go/natscontext"
	"github.com/nats-io/nats.go"
)

func main() {
	// connect to rfid reader
	chanEPC, reader := connectRFID()
	defer reader.Close()

	// connect to nats
	nc := connectNATS("nats_development")
	defer nc.Close()

	// publish epc to nats
	for epc := range chanEPC {
		err := nc.Publish(fmt.Sprintf("rfid.epc.%s", epc), []byte(strings.ToUpper(epc)))
		check(err)
	}

}

// check panics if err is not nil
func check(err error) {
	if err != nil {
		panic(err)
	}
}

// connect to nats using context name and returns a nats connection
func connectNATS(name string) *nats.Conn {
	nc, err := natscontext.Connect(name)
	check(err)
	return nc
}

// connectRFID connects to the RFID reader and returns a channel to receive EPCs on
func connectRFID() (chan string, *urmsp.Client) {
	reader, err := urmsp.SerialAuto()
	check(err)

	// change reader mode
	check(reader.AISpecStopTrigger())
	check(reader.StartROSpec())

	// create a channel to report back into
	chanEPC := make(chan string)

	// handle epc96 messages
	reader.HandleEPC96(func(ctx context.Context, data []byte) {
		// convert data to hex string
		chanEPC <- hex.EncodeToString(data)
	})

	// return channel and reader
	return chanEPC, reader
}
