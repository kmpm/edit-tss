





## Prep
__start a server__
```shell
# start a server
nats run server --jetstream
```

__start a service that replies to 'service.weather.>'__
```shell
# respond to weather requests
nats --context nats_development reply 'service.weather.>' --command "curl -s wttr.in/{{2}}?format=3"
```

__start programs__
```shell
# start reader
go run ./cmd/tagreader

# start counter
go run ./cmd/counter

# start weather-client
go run ./cmd/weather

